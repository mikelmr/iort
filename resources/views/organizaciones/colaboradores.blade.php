@extends('layouts.app')

@push('styles')
  <link href="{{ URL::asset('css/colaboradores.css') }}" rel="stylesheet">
@endpush


@section('content')

  <h1>Colaboradores</h1>
  <div class="row">
    @foreach ($organizaciones as $organizacion)
      @if ($organizacion->logo)
      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 colaboradores_caja">
        <span class="colaboradores_helper"></span>
        @if ($organizacion->pagina_web)
          <a target="_blank" rel="noopener noreferrer nofollow" href="{{ $organizacion->pagina_web }}">
        @endif
            <img src="{{ '/images/logos/' . $organizacion->logo }}" class="colaboradores_imagen" />
        @if ($organizacion->pagina_web)
          </a>
        @endif
      </div>
      @endif
    @endforeach
  </div>
@endsection

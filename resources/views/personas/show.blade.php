@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-sm-6">
    <h1>Datos Persona</h1>
    <dl class="row">
      <dt class="col-sm-4">Fecha de alta:</dt>
      <dd class="col-sm-8">{{ $persona->created_at->toDateString() }}</dd>
      <dt class="col-sm-4">Nombre:</dt>
      <dd class="col-sm-8">{{ $persona->nombre }}</dd>
      <dt class="col-sm-4">Apellido 1:</dt>
      <dd class="col-sm-8">{{ $persona->apellido_1 }}</dd>
      <dt class="col-sm-4">Apellido 2:</dt>
      <dd class="col-sm-8">{{ $persona->apellido_2 }}</dd>
      <dt class="col-sm-6">Fecha de Nacimiento:</dt>
      <dd class="col-sm-6">{{ $persona->fecha_nacimiento }}</dd>
      <dt class="col-sm-4">Sexo:</dt>
      <dd class="col-sm-8">
        @if ($persona->sexo)
          {{ $persona->sexo->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Dirección:</dt>
      <dd class="col-sm-8">{{ $persona->direccion }}</dd>
      <dt class="col-sm-4">CP:</dt>
      <dd class="col-sm-8">{{ $persona->cp }}</dd>
      <dt class="col-sm-4">Población:</dt>
      <dd class="col-sm-8">{{ $persona->poblacion }}</dd>
      <dt class="col-sm-4">Provincia:</dt>
      <dd class="col-sm-8">
        @if ($persona->provincia)
          {{ $persona->provincia->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Teléfono 1:</dt>
      <dd class="col-sm-8">{{ $persona->telefono_1 }}</dd>
      <dt class="col-sm-4">Teléfono 2:</dt>
      <dd class="col-sm-8">{{ $persona->telefono_2 }}</dd>
      <dt class="col-sm-4">Email:</dt>
      <dd class="col-sm-8">{{ $persona->email }}</dd>
      <dt class="col-sm-4">Página Web:</dt>
      <dd class="col-sm-8">{{ $persona->pagina_web }}</dd>
      <dt class="col-sm-6">¿Cómo nos has conocido?:</dt>
      <dd class="col-sm-6">
        @if ($persona->tipoConocido)
          {{ $persona->tipoConocido->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Tipo de alta:</dt>
      <dd class="col-sm-8">
        @if ($persona->tipoAlta)
          {{ $persona->tipoAlta->nombre }}
        @endif
      </dd>
      <dt class="col-sm-4">Organización:</dt>
      <dd class="col-sm-8">
        @if ($persona->organizacion)
          {{ $persona->organizacion->razon_social }}
        @endif
      </dd>
      <dt class="col-sm-4">Cargo:</dt>
      <dd class="col-sm-8">{{ $persona->cargo }}</dd>
      <dt class="col-sm-4">Notas:</dt>
      <dd class="col-sm-8">{{ $persona->notas }}</dd>
    </dl>
  </div>
  <div class="col-sm-6">
    <h2>Lotes</h2>
    <ul>
      @foreach ($persona->lotes as $lote)
        <li><a href="{{ url('lotes/' . $lote->id )}}">{{ $lote->fecha->format('d/m/Y') . ' - ' . $lote->descripcion }}</a></li>
      @endforeach
    </ul>
  </div>
</div>
  <div class="row" style="clear:both;">
    <div class="col-sm">
      <a href="{{ url('personas/' . $persona->id . '/edit')}}" class="btn btn-primary">Editar</a>
    </div>
    <div class="col-sm">
      <a href="{{ action('LoteController@create').'?'.http_build_query(["tipo" => get_class($persona), "id" => $persona->id]) }}" class="btn btn-primary">Nuevo</a>
    </div>
  </div>

@endsection

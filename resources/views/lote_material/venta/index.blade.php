<div>
    <table class="table">
        <thead>
            @if (isset($edicion) && $edicion)
            <tr>
                <th>Material</th>
                <th>Código</th>
                <th>Descripción</th>
                <th>Precio</th>
                <th>Descuento</th>
                <th></th>
            </tr>
            <tr>
                <td>{{ Form::select('material_id', $materiales, null, ['id'=> 'material_id', 'class'=>"form-control", 'placeholder' => ''])}}</td>
                <td>{{ Form::text('material_codigo', null, ['id'=> 'material_codigo', 'class' =>"form-control"] )}}</td>
                <td>{{ Form::text('material_descripcion', null, ['id'=> 'material_descripcion', 'class' =>"form-control"] )}}</td>
                <td>{{ Form::text('material_precio', null, ['id'=> 'material_precio', 'class' =>"form-control"] )}}</td>
                <td>
                    <div class="row">              
                        {{ Form::text('material_descuento', null, ['id'=> 'material_descuento', 'class' =>"form-control col-sm-8"] )}}
                        {{ Form::select('material_tipo_descuento', ['%' => '%', '€' => '€'], null, ['id'=> 'material_tipo_descuento', 'class'=>"form-control col-sm-4", 'default'=>'%'])}}
                    </div>
                </td>
                <td>
                    {{ Form::hidden('material_entrada_salida', 1,['id'=> 'material_entrada_salida']) }}
                    <button type="button" class="btn btn-success add-loteMaterial">Añadir</button>
                </td>
            </tr>
            <tr class="alert alert-danger print-error-msg" style="display:none">
                <td colspan="7">
                    <ul></ul>
                </td>
            </tr>
            @endif
            <tr><th colspan="7">Resumen:</th></tr>
            <tr>
                <th>Material</th>
                <th>Código</th>
                <th>Descripción</th>
                <th>Precio</th>
                <th>Descuento</th>
                <th>Total</th>
                @if (isset($edicion) && $edicion)
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($loteMateriales as $loteMaterial)
            <tr id="loteMaterial{{$loteMaterial->material_id}}">
                <td>{{ $loteMaterial->material->nombre }}</td>
                <td>{{ $loteMaterial->codigo }}</td>
                <td>{{ $loteMaterial->descripcion }}</td>
                <td>{{ $loteMaterial->precio }}</td>
                <td>-{{ $loteMaterial->descuentoFinal }}</td>
                <td>{{ $loteMaterial->precioFinal }}</td>
                @if (isset($edicion) && $edicion)
                <td>
                    <button type="button" class="btn btn-danger delete-loteMaterial" value="{{$loteMaterial->id}}">Eliminar</button>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
function printErrorMsg(msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display', '');
    $.each(msg.errors, function (key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}

function cargarJavascriptElementos() {
    cargarBotonesAniadir();
    cargarBotonesEliminar();

    $('#material_id').select2({
        placeholder: 'Selecciona el material..',
    });
}

function cargarBotonesEliminar() {
    $(".delete-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            $.ajax({
                type: "DELETE",
                url: "{{ url('lotes/' . $lote->id .'/loteMateriales/')}}/" + $(this).attr('value'),
                dataType: "json",
                success: function (data) {
                    if (data && data.success) {
                        $('#loteMaterialIndex').html(data.html);
                        cargarJavascriptElementos();
                    } else {
                        alert(data.message);
                    }
                }
            });
        });
    });
}

function cargarBotonesAniadir() {
    $(".add-loteMaterial").each(function (index, item) {
        $(this).unbind("click");
        $(this).on("click", function (event) {
            $(".print-error-msg").css('display', 'none');
            var formData = {
                lote_id: <?php echo $lote->id; ?>,
                material_id: $('#material_id').val(),
                codigo: $('#material_codigo').val(),
                descripcion: $('#material_descripcion').val(),
                precio: $('#material_precio').val(),
                tipo_descuento: $('#material_tipo_descuento').val(),
                descuento: $('#material_descuento').val(),
                entrada_salida: $('#material_entrada_salida').val()
            }
            $.ajax({
                type: "POST",
                url: "{{ url('lotes/' . $lote->id .'/loteMateriales')}}",
                dataType: "json",
                data: formData,
                success: function (data) {
                    if (data && data.success) {
                        $('#loteMaterialIndex').html(data.html);
                        $(':input', '#loteMaterialIndex')
                                .not(':button, :submit, :reset, :hidden')
                                .val('').change()
                                .removeAttr('checked')
                                .removeAttr('selected');
                        cargarJavascriptElementos();
                    } else {
                        alert(data.message);
                    }
                },
                error: function (data, textStatus, errorThrown) {
                    printErrorMsg(data.responseJSON);
                }
            });
        });
    });
}

jQuery(document).ready(function ($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    cargarJavascriptElementos();
});


</script>
@endpush

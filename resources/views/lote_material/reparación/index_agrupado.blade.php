@php
    $loteMaterialesFiltrados = $lote->materiales->groupBy('entrada_salida');
    $valorSalvarInformacion = '';
    if($loteMaterialesFiltrados->has('')
        && count($loteMaterialesFiltrados['']) == 1 
        && $loteMaterialesFiltrados[''][0]->descripcion == 'Salvar Informacion') 
        {
            $valorSalvarInformacion = $loteMaterialesFiltrados[''][0]->id;
        }
@endphp

@if ($loteMaterialesFiltrados->has(0))
<table style="width: 100%;">
    <tr>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Descripción</th>
    </tr>
    <tr class="hr"><td colspan="3"></td></tr>

    @foreach ($loteMaterialesFiltrados[0] as $material)
    <tr>
        <td>{{ $material->marca }}</td>
        <td>{{ $material->modelo }}</td>
        <td>{{ $material->descripcion }}</td>
    </tr>
    <tr class="hr"><td colspan="3"></td></tr>
    @endforeach
</table>
@endif

<h3 style='padding: 1em 0;'>Requiere guardar información: {{ $valorSalvarInformacion ? 'Sí' : 'No'}}</h3>

<h2>Trabajos realizados:</h2>

<table style="width: 100%;">
    <tr>
        <th>Descripcion</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Total</th>
    </tr>
    <tr class="hr"><td colspan="3"></td></tr>
    @php
        $total = 0;
    @endphp

    @foreach ($lote->getMaterialesAgrupados(['descripcion', 'precio'], ['entrada_salida' => 1]) as $material)
    <tr>
        <td>{{ $material->descripcion }}</td>
        <td class="text-center">{{ $material->precio }}€</td>
        <td class="text-center">{{ $material->cantidadSuma }}</td>
        <td class="text-center">{{ $material->precioSuma }}€</td>
    </tr>
    <tr class="hr"><td colspan="3"></td></tr>
    @php
    $total += $material->precioSuma
    @endphp
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td class="text-right"><strong>Total:</strong></td>
        <td class="text-center"><strong>{{ $total }}€</strong></td>
    </tr>
</table>

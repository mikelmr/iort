<table style="width: 100%;">
    <tr>
        <th>Descripcion</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Total</th>
    </tr>
    <tr class="hr"><td colspan="3"></td></tr>
    @php
    $total = 0;
    @endphp

    @foreach ($lote->getMaterialesAgrupados(['descripcion', 'precio', 'descuento']) as $material)
    <tr>
        <td class="text-center">{{ $material->descripcion }}</td>
        <td class="text-center">{{ $material->precio }}€</td>
        <td class="text-center">{{ $material->cantidadSuma }}</td>
        <td class="text-center">{{ $material->precioSuma }}€</td>
    </tr>
    @if ($material->descuentoSuma > 0)
    <tr class="hr"><td colspan="3"></td></tr>
    <tr>
        <td class="text-center">{{ $material->precioSuma == $material->descuentoSuma ? 'Donado' : 'Descontado'  }}</td>
        <td class="text-center"></td>
        <td class="text-center"></td>
        <td class="text-center">- {{ $material->descuentoSuma }}€</td>
    </tr>
    @endif
    <tr class="hr"><td colspan="3"></td></tr>
    @php
    $total += $material->precioSuma - $material->descuentoSuma
    @endphp
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td class="text-right"><strong>Total:</strong></td>
        <td class="text-center"><strong>{{ $total }}€</strong></td>
    </tr>
</table>

<div id="loteMaterialIndex">
  <h3 class="col-md-8">Lote material</h3>
  <div>
    <?php 
        $loteMateriales = $lote->materiales()->paginate(10);
        $loteMateriales->setPath('');
    ?>
    @include ("lote_material.".strtolower($lote->tipoLote->nombre).".index")
    {{ $loteMateriales->links() }}
  </div>
</div>

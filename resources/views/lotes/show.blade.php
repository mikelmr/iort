@extends('layouts.app')

@section('content')
  <div class="col-sm-12">
    <h1>Datos Lote</h1>
    <dl class="row">
      <dt class="col-sm-3">Fecha:</dt>
      <dd class="col-sm-3">{{ $lote->fecha->format('d/m/Y') }}</dd>
      <dt class="col-sm-3">Tipo:</dt>
      <dd class="col-sm-3">{{ $lote->tipoLote->nombre }}</dd>      
      <dt class="col-sm-3">Descripcion:</dt>
      <dd class="col-sm-9">{{ $lote->descripcion }}</dd>
    </dl>
  </div>

  @include ('lote_material.index_agrupado')

  <div style="clear:both;">
    <div class="col-sm-6">
      <a href="{{ url('lotes/' . $lote->id . '/edit')}}" class="btn btn-primary">Editar</a>
      <a href="{{ url('lotes/' . $lote->id . '/informe')}}" class="btn btn-primary" target="_blank">Imprimir</a>
      <a href="{{ url('labels/export/?lote=' . $lote->id )}}" class="btn btn-primary" target="_blank">Exportar</a>
    </div>
  </div>
@endsection

@extends('layouts.app')

@section('content')
<div id="loteMateriales">
    <h1>Materiales Lotes</h1>
    <div>
        <form class="float-right" role="search" method="GET">
            <div class="input-group mb-3">
                <input type="text" name="q" class="form-control col-sm-6" placeholder="Buscar..." value="{{ request('q') }}">
                <div class="input-group-append">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
                        <i class="fa fa-search fa-lg"></i>
                        Buscar
                    </button>
                </div>
            </div>
        </form>

        <table class="table">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Responsable</th>
                    <th>Fecha</th>
                    <th>Material</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($loteMateriales as $loteMaterial)
                <tr>
                    <td>{{ $loteMaterial->codigo }}</td>
                    <td>{{ $loteMaterial->lote->responsable->codigo }}</td>
                    <td>{{ $loteMaterial->lote->fecha->format('Ymd') }}</td>
                    <td>{{ $loteMaterial->material->nombre }}</td>
                    <td>
                        <button type="button" class="btn btn-danger txae-loteMaterial" codigo="{{$loteMaterial->codigo}}" value="{{$loteMaterial->id}}">Marcar Txae</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $loteMateriales->appends($_GET)->links() }}
    </div>
</div>
@endsection

@push('scripts')
<script>

    function cargarBotonesMarcarTxae() {
        $(".txae-loteMaterial").each(function (index, item) {
            $(this).unbind("click");
            $(this).on("click", function (event) {
                if (!confirm('Estás seguro de que quieres marcar el código ' + $(this).attr('codigo') + ' como Txae?')) {
                    event.preventDefault();
                    return;
                }

                $.ajax({
                    type: "PATCH",
                    url: "{{ action('TxaeController@update') }}",
                    dataType: "json",
                    data: {
                        id: $(this).attr('value'),
                        codigo: $(this).attr('codigo')
                    },
                    success: function () {
                        location.reload();
                    }
                });
            });
        });
    }

    jQuery(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        cargarBotonesMarcarTxae();
    });


</script>
@endpush

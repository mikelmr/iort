<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @stack('styles')
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <!-- Left Side Of Navbar -->
                <div class="navbar-left">
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('images/logo.png') }}" alt="logo" class="logo" />
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <!-- Right Side Of Navbar -->
                <div class="navbar-right form-inline">
                    <!-- Collapsed Hamburger -->
                    <div class="navbar-dark bg-light hamburger">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span style="color:black;"><i class="fas fa-bars fa-1x"></i></span>
                        </button>
                    </div>

                    <ul class="nav navbar-brand">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Conectarse</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>

            <nav class="sidenav">
                <ul class="navbar-collapse collapse show" id="app-navbar-collapse">
                    <li class="{{ Request::is('/')? 'active' : ''}}"><a href="{{ url('/')}}">Inicio</a></li>
                    <li class="{{ Request::is('personas')? 'active' : ''}}"><a href="{{ url('personas')}}">Personas</a></li>
                    <li class="{{ Request::is('organizaciones')? 'active' : ''}}"><a href="{{ url('organizaciones')}}">Organizaciones</a></li>
                    <li class="{{ Request::is('lotes')? 'active' : ''}}"><a href="{{ url('lotes')}}">Lotes</a></li>
                    <li class="{{ Request::is('txaes')? 'active' : ''}}"><a href="{{ action('TxaeController@index')}}">Txaes</a></li>
                    <li class="{{ Request::is('colaboradores')? 'active' : ''}}"><a href="{{ url('colaboradores')}}">Logos</a></li>
                    <li class="{{ Request::is('informes')? 'active' : ''}}"><a href="{{ url('informes')}}">Informes</a></li>
                 </ul>
                <div>

                </div>
            </nav>

            <div class="content">
                @if (isset($breadcrumbs) && count($breadcrumbs))
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        @foreach ($breadcrumbs as $breadcrumb)
                        @if (array_key_exists('url', $breadcrumb))
                        <li class="breadcrumb-item"><a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a></li>
                        @else
                        <li  class="breadcrumb-item active">{{ $breadcrumb['title'] }}</li>
                        @endif
                        @endforeach
                    </ol>
                </nav>
                @endif
                @yield('content')
                <flash></flash>
            </div>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        @stack('scripts')

        @if ($flash = session('flash'))
        <script>
            @if (is_array($flash))
                flash('{{ $flash['message'] }}', '{{ $flash['level'] }}');
            @elseif (is_string($flash))
                flash('{{ $flash }}');
            @endif
        </script>
        @endif
    </body>
</html>

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TipoLoteAniadirTipoReparacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         $tipoLote = [
           ['id' => 6, 'nombre' => 'Reparación'],
         ];

         DB::table('tipo_lote')->insert($tipoLote);
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         DB::table('tipo_lote')->delete(6);
     }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoteMaterialAddEsEntrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lote_materiales', function (Blueprint $table) {
           $table->boolean('entrada_salida')->nullable();
           
           $table->unique(['codigo', 'entrada_salida']);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lote_materiales', function (Blueprint $table) {
           $table->dropColumn('entrada_salida');
       });
    }
}

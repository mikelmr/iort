<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmptyWith implements Rule
{

    var $other;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($other)
    {
        $this->other = $other;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $other = request()->input($this->other);

        return !($other && !empty($value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return str_replace(':other', $this->other,
                trans('validation.empty_with'));
    }
}

<?php

namespace App;

use Illuminate\Support\Facades\DB;

class LoteMaterial extends Model
{
    protected $table = 'lote_materiales';
    
    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['material', 'lote'];

    public function lote()
    {
        return $this->belongsTo(Lote::class);
    }

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public static function getCodigoSiguiente()
    {
        $codigo = DB::table('lote_materiales')
          //->whereYear('created_at', ''.Carbon::today()->year)
          ->max('codigo');

        return $codigo ? $codigo + 1 : 1;
    }

    public function getDescuentoFinalAttribute()
    {
      return $this->donacion ? $this->precio : $this->descuento;
    }
    
    public function getPrecioFinalAttribute()
    {
      return $this->precio - $this->descuentoFinal;
    }

    public function scopeEntrada($query)
    {
      return $query->whereHas('lote.tipoLote', function ($query) {
          $query->whereNombre('Entrada');
      });
    }

    public function scopeNoTxae($query)
    {
      return $query->where(function ($query) {
          $query->whereNull('txae')
                ->orWhere('txae','=','0');
      });
    }

    public function scopeVirtual($query)
    {
      return $query->where(function ($query) {
          $query->where('virtual','=','1');
      });
    }

    public function scopeFisico($query)
    {
      return $query->where(function ($query) {
          $query->where('virtual','=','0');
      });
    }
    
    public function esEntrada()
    {
        return $this->entrada_salida == 0;
    }
    
    public function esSalida()
    {
        return $this->entrada_salida == 1;
    }
    
    public function filtrar($filtros = [])
    {
        $cumple = true;
        
        foreach ($filtros as $filtro => $valor){
            $cumple &= $this->$filtro == $valor;
        }
        
        return $cumple;
    }
}

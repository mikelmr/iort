<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoteMaterialRequest;
use App\Lote;
use App\LoteMaterial;
use App\Material;

class LoteMaterialController extends Controller
{

    public function store(Lote $lote, LoteMaterialRequest $request)
    {
        $lote->addLoteMaterial(
                $request->except(['cantidad', 'tipo_descuento']),
                $request->input('cantidad', 1),
                $request->input('tipo_descuento', null)
        );

        $returnHTML = view('lote_material.index')
                ->with('lote', $lote)
                ->with('materiales',
                        Material::orderBy('nombre')->pluck('nombre', 'id'))
                ->with('edicion', true)                
                ->render();

        return response()->json(['success' => true, 'html' => $returnHTML]);
    }

    public function destroy(Lote $lote, LoteMaterial $loteMaterial)
    {
        $loteMaterial->delete();

        $returnHTML = view('lote_material.index')
                ->with('lote', $lote)
                ->with('materiales',
                        Material::orderBy('nombre')->pluck('nombre', 'id'))
                ->with('edicion', true)
                ->render();
        return response()->json(['success' => true, 'html' => $returnHTML]);
    }
}

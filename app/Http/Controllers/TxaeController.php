<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoteMaterial;
use App\Filters\TxaeFilters;

class TxaeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(TxaeFilters $filters)
    {        
        $query = LoteMaterial::entrada()
            ->noTxae()
            ->orderBy('codigo', 'desc');
        $filters->apply($query);

        $loteMateriales = $query->paginate();

        return view('txaes.index', compact('loteMateriales'));
    }

    public function edit()
    {
        return view('txaes.edit');
    }

    public function update()
    {
        $this->validate(request(), [
          'id' => 'required|numeric',
        ]);
        $material = LoteMaterial::Entrada()
          ->whereId(request()->input('id'))
          ->noTxae()
          ->first();

        $level = 'danger';
        $message = "El código no ha podido marcarse como Txae.";

        if ($material) {
          $material->txae = 1;
          $material->save();
          $level = 'success';
          $message = "El código ha sido marcado como Txae.";

        }

        session()->flash('flash', ['message' => $message, 'level' => $level]);

        if(request()->ajax()){
            return response()->json(['success' => true]);
        }

        return back();
    }
}

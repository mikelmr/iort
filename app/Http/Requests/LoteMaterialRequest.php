<?php

namespace App\Http\Requests;

use App\Rules\EmptyWith;
use Illuminate\Foundation\Http\FormRequest;

class LoteMaterialRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'material_id' => 'required',
            'cantidad' => 'numeric',
            'codigo' => 'numeric|nullable',
            'precio' => 'nullable|required_with:descuento|numeric',
            'donacion' => 'boolean',
            'tipo_descuento' => 'nullable|required_with:descuento|in:%,€',
            'descuento' => ['nullable',
                'numeric',
                new EmptyWith('donacion')]
        ];

        switch ($this->request->get('tipo_descuento')) {
            case '%':
                $rules['descuento'][] = 'between:1,99';
                break;
            case '€':
                $rules['descuento'][] = 'lt:precio';
                $rules['descuento'][] = 'min:0';
                break;
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'material_id.required' => 'Hay que seleccionar un tipo de material',
            'descuento.lt' => 'El descuento no puede ser mayor que el precio',
        ];
    }
}

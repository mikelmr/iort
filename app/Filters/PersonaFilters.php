<?php

namespace App\Filters;

class PersonaFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['q'];
    
    protected function q($patron)
    {
        $q = "%";
        if ($patron) {
            $q = '%' . $patron . '%';
        }
        
        $this->builder
            ->where('nombre', 'like', $q)
            ->orWhere('apellido_1', 'like', $q)
            ->orWhere('apellido_2', 'like', $q)
            ->orWhere('poblacion', 'like', $q)
            ->orWhere('telefono_1', 'like', $q)
            ->orWhere('email', 'like', $q);           
    }
}
